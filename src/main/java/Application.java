import org.openjdk.jol.info.GraphLayout;

public class Application {
    public static void main(String[] args) {
        String s = new String("wow1wow1wow1wow1wow1wow1");

        for (int i = 0; i < 1000; i++) {
            GraphLayout.parseInstance(String.valueOf(i)).toFootprint();
        }
        long t = System.currentTimeMillis();

        for (int i = 0; i < 200000; i++) {
            GraphLayout.parseInstance(String.valueOf(i)).totalSize();
        }
        System.out.println(System.currentTimeMillis() - t);
    }
}
